# COLA2 Conventions

[TOC]

In this document you will find a compendium of the naming conventions that have been followed throughout the implementation of COLA2.

## Namings and namespaces

- When naming packages, nodes, topics and messages inside COLA2, the general [ROS conventions](http://wiki.ros.org/ROS/Patterns/Conventions) are used.

- The name for device driver packages follow the pattern `manufacturer_[model]_device`, e.g., `imagenex_deltat_multibeam`.

- If a message definition contains, for some reason, fields that are not in the SI, the field specifies the units explicitly after a trailing underscore (e.g., `rotation_degrees`). Otherwise it is understood that the units are the SI ones.

- All nodes that are launched from COLA2 main launch files (in the vehicle package), are launched under the vehicle namespace, i.e., `/<vehicle>/node_name`.

- Private node handlers (`~`) are used in the majority of nodes. Therefore, topics, services and parameters related to a given node are declared inside it using:

    - **Python**: the `~/topic_name`, `~/service_name` or `~/param_name`

    - **C++**: `ros::NodeHandle("~")` and names as `topic_name`, `service_name` or `param_name`

    and they automatically resolve to the absolute names `/<vehicle>/node_name/topic_name`, `/<vehicle>/node_name/service_name` and `/<vehicle>/node_name/param_name`.

    An exception to the above are the sensor topics that are remapped in the launch files to be the input to the navigator node. Although they are published from a device driver node (e.g., `/<vehicle>/teledyne_explorer_dvl/dvl` the topic is then remapped to `/<vehicle>/navigator/dvl`).

- Configuration YAML files for a given node are named `node_name.yaml` and its parameters do not include the node namespace, only the parameter name.

## Robot frames and coordinate systems

All frames in the vehicle are related in COLA2 using the standard ROS TF library. This library internally maintains a time buffered tree of transformations and eases the task of computing the transformation between any two frames.

The COLA2 world reference frame corresponds to the `world_ned` TF. To allow proper visualization in the RViz a frame named `world` is also created. This helper frame is rotated 180 degrees in the x axis with respect to the `world_ned` and it is configured as a fixed frame in RViz. This effectively rotates the visualization so that the NED frame is properly represented.

Finally, the COG of the vehicle is always represented using the name `<vehicle>/base_link`. The remaining frames of the robot sensors and actuators are also located inside its namespace and defined with respect to the `<vehicle>/base_link`. All these transformations are defined with the help of xacro macros in the `<vehicle>_description` package.

## Package structure

COLA2 c++ packages follow the structure shown in Figure 1.

![Tree view of typical package structure.](imgs/package_structure.png)

Basic code, uncoupled from the ROS, is found in the `include` (for headers) and `src` (for source) folders, inside `pkg_name` subfolders. The ROS nodes using these code are found directly in `src` folder, and identified by the trailing `_node`. Whenever the node requires to load parameters from the ROS param server, an example configuration YAML is provided in the `config` folder. Similarly, a basic launch file to load the required parameters and launch the node is provided in the `launch` folder.

Note that these are just config and launch files accompanying the package for example purposes. When executing COLA2 on the real vehicle or simulating it, the config files that are actually used are the ones residing in the vehicle package (`cola2_<vehicle>`). If the node requires custom ROS messages, for instance for storing a set of raw data from a sensor, the corresponding message definitions are found inside the `msg` folder. In case the message definition is needed by other COLA2 nodes, the .msg file should be placed in the `cola2_msgs` package. Python packages follow a similar structure, although typically they just contain the ROS nodes inside the `src` folder.

## Coding conventions

When developing code for COLA2, these are some guidelines to take into account:

- COLA2 c++ packages follow c++11 standard.

- When possible, try to follow the ROS Style guide for [c++](http://wiki.ros.org/CppStyleGuide) (clang-format) and [python](http://wiki.ros.org/PyStyleGuide).

- Functions from `cola2_lib` and `cola2_lib_ros` should be employed for tasks that are common across COLA2 nodes (e.g., reading parameters from the ROS param server, setting up diagnostics, converting coordinates, etc.). Have a look at the API of `cola2_lib` and `cola2_lib_ros` in [Iqua API website](http://api.iquarobotics.com/).
