# Basic usage

[TOC]

COLA2 can be run in Iqua vehicles or in simulation, with simulated dynamics and sensors.

The main launch files that control which ROS nodes are launched and which parameters are loaded, are located inside `cola2_<vehicle>/launch`. These are the commands to launch the real and the simulated software architecture of \<vehicle\> AUV:

```bash
roslaunch cola2_<vehicle> start.launch      # Inside the real vehicle
roslaunch cola2_<vehicle> sim_start.launch  # Simulated
```

These launch files, together with other launch files that are called from within these main ones, contain several configurable arguments that can be enabled or disabled to control which ROS nodes are launched and which parameters are loaded into the ROS param server. Therefore you can easily configure which payload sensors should be enabled/disabled.

The different configuration parameters required by the nodes can be set in several YAML files. All configuration files are placed in the configuration folder of your vehicle package i.e., `cola2_<vehicle>/config/` when working with the vehicle or `cola2_<vehicle>/sim_config/` when working in simulation. Therefore, before starting the main launch file you should be sure that you have the appropriate parameters for the mission you intend to do. This typically implies to check and modify, if required, the parameters concerning navigation (`navigator.yaml`), safety (`safety_supervisor.yaml`) and the enabled payload sensors. Details on important parameters to check will be given throughout the corresponding sections of this manual.

After starting the main architecture launch, you can interact with it in several ways, as explained in the next section.

## Interfaces

Once COLA2 is running, you can interact with it in several ways:

- **ROS terminal tools:** ROS offers a series of command-line utilities to display runtime information or perform certain interactions from the terminal. For instance, you can check any published topic through the `rosptopic echo topic_name` command, check information about a particular node with `rosnode info node_name` or execute any service with `rosservice call service_name service_params`.

- **ROS graphical tools:** ROS provides several graphical utilities that can help you monitor your vehicle, test, debug and develop new functionalities. The most popular ones are:

	- **rviz** is a 3D visualization environment that lets you combine sensor data, robot model, and other 3D data into a combined view.

	- **rqt\_bag** is a graphical tool for viewing data contained in ROS bag files.

	- **robot\_monitor** provides a GUI that displays the diagnostics data of a vehicle in a tree form, useful to see errors and warnings in a categorized way.

	- **rqt\_graph** displays a visual graph of the processes running in ROS and their connections.

	- **rqt\_tf\_tree** provides a GUI for visualizing the ROS TF frame tree, and thus see the relationship between the different coordinate frames of the vehicle.

	- **rqt\_plot** plots numerical data of a ROS topic over time.

	- **dynamic\_reconfigure** package provides a means to change node parameters at any time without having to restart the node. Is a way to expose a subset of a node’s parameters to external reconfiguration.

- **Third party simulators:** In order to work with 3D simulated environments COLA2 has been used in conjunction of simulators such as [Gazebo](http://gazebosim.org/) or [Stonefish](https://github.com/patrykcieslak/stonefish). However, please note that these are third-party simulators and Iqua Robotics does not provide any support on its integration and interaction with COLA2.

- **IQUAView:** Iqua Robotics provides a GIS-based graphical user interface for controlling and operating AUVs running the COLA2 architecture. IQUAView handles the interaction with COLA2 through the rosbridge protocol and provides a graphical front-end to change parameters in the rosparam server, call services and display vehicle information contained into published rostopics. It allows in a user-friendly manner to conduct pre-mission checks, configure vehicle parameters, plan missions and monitor the vehicle during mission execution. You can find more information about it [here](https://bitbucket.org/iquarobotics/iquaview).
