# Logging

[TOC]

The logging functionality of COLA2 is mainly based in the ROS bag system. The `cola2_log` package contains nodes related to the logging/storing COLA2 parameters and data. Essentially there are three types of bags that can be recorded:

- Bags recorded manually: those produced by calling `rosbag record` from the terminal. These bags are stored where the command is issued.

- Bags recorded through the services of `bag_node`. The bag\_node provides services to start (`/<vehicle>/bag_recorder/enable_logs`) and stop (`/<vehicle>/bag_recorder/disable_logs`) a launch file that is in charge of recording bags, placed in `cola2_<vehicle>/launch`. By default, this launch file records one bag that has all topics under the file pattern `<vehicle>_YYYY-MM-DD-HH-mm-ss_n.bag` in the `~/bags` folder, but you can easily configure it to exclude topics or record more than one bag at a time with particular topics in each one. This is convenient for having all the information of an experiment separated from the data that is needed, for instance, for image processing, thus avoiding to deal with large bags when it is not needed. Besides, the bag.launch file takes care of splitting the bags by size (by default 2Gb).

- Basic bags. When COLA2 starts, one of the launch files that are started from the main one is the `/<vehicle>/launch/basic_bag.launch`, which starts a bag recording of basic navigation, control and safety topics at a low frequency. These basic bags are stored in the `bags` folder under the name `<vehicle>_basic_YYYY-MM-DD-HH-mm-ss_n.bag`.

Aside from the bags, logs from some device drivers such as the battery management system or raw data files recorded from some sensors (e.g., multibeam data) are stored in the folder `~/logs/` inside a corresponding subfolder with the name of the device package.

Together with the `bag_recorder`, other nodes available under `cola2_log` are:

- `param_logger`: publishes all ROS parameters present at the moment of starting the COLA2 in a topic of type `std_msgs/String` for logging/debugging purposes. The topic is published in latched mode so that when a rosbag record is started it always gets the message that was published. In this way, it is always possible to check the COLA2 configuration parameters that were set for an specific experiment.

- `computer_logger`: publishes cpu and RAM usage data and temperature from the vehicle computer for logging purposes.

- `default_param_handler`: provides a service to store current ROS parameters as defaults writing them to the corresponding YAML files. The service, (`/<vehicle>/default_param_handler/update_params_in_yamls`), will update the YAML files in the folder pointed by the parameters `default_param_handler/config_pkg` and `default_param_handler/config_folder` (e.g., `cola2_<vehicle>` and `sim_config`).

- `cola2_version`: reports the status of the used IQUA repositories in the current `catkin_ws`.

- `mission_reporter`: reports mission success or failure with some statistics for each executed mission.
