# Installation

[TOC]

Before starting to work with the COLA2 architecture, it is essential to have a good knowledge of ROS as well as some Linux skills. In the ROS [website](http://www.ros.org) you can find several tutorials that can help you getting started. You should be familiar with the ROS package structure, the ROS nomenclature, the publish/subscribe and client/server mechanisms and the basic ROS tools. We also recommend you to learn some [GIT](http://www.git-scm.com) basics.

The following steps will guide you to install a system ready to run COLA2. Although COLA2 is already installed inside your vehicle and in your control station, you may want to install it in another computer to run it in simulation.

## System prerequisites

The recommended setup to run the current version of the COLA2 architecture is [Ubuntu 20.04 LTS](http://releases.ubuntu.com/20.04/) and [ROS Noetic](http://wiki.ros.org/noetic).

We recommend to install the ROS desktop-full version (`ros-noetic-desktop-full`).

## Extra dependencies

Aside from the main ROS packages, there are other ROS packages and third-party libraries that are required to run the basics of COLA2. Moreover, depending on the configuration of your vehicle and the equipped sensors, you might need to install further dependencies. For instance, if you work with camera images you might want the `ros-noetic-compressed-image-transport` package to be able to use compressed images. In case that a COLA2 driver package requires a specific package or library to be installed it will be listed inside the `README.md` of the corresponding package.

The basic dependencies are the following ones.

### Ubuntu 20.04

```bash
sudo apt install ros-noetic-rosbridge-server ros-noetic-joy lm-sensors lcov python3-ruamel.yaml python-is-python3 python3-pip
pip3 install GitPython==3.1.42
```

## COLA2 packages

The COLA2 architecture is formed by at least 5 ROS packages:

- `cola2_msgs`: includes message definitions used within the architecture.

- `cola2_core`: a metapackage containing the code of the main modules that are common to all Iqua Robotics vehicles, namely the packages `cola2_control`, `cola2_log`, `cola2_nav`, `cola2_safety`, `cola2_comms` and `cola2_sim`.

- `cola2_lib_ros`: includes utils used within the architecture that have to do with ROS: classes to ease the ROS param loading, the diagnostics publication, etc.

- `cola2_<vehicle>`: includes the configuration and launch files specific to your vehicle.

- `<vehicle>_description`: includes the xacro macros to build the robot description in URDF of the vehicle and the mesh models used for simulation.

There is also an extra package, `cola2_lib` which is not a ROS package and includes general utils used within the architecture: input/output libraries, conversion functions, etc. Since this package is ROS agnostic it should be compiled and installed outside your catkin workspace.

```bash
git clone https://bitbucket.org/iquarobotics/cola2_lib.git
cd cola2_lib
# check the required dependencies and then
mkdir build
cd build
cmake ..
make
sudo make install
```

Once ROS and `cola2_lib` are installed, you can proceed to install the COLA2 packages inside the catkin workspace `~/catkin_ws/src`, that you have created during the ROS installation (if you don’t have a catkin workspace, follow the instructions [here](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)).

For simulation purposes you will need only the 5 packages listed before. If you have a real vehicle, you will need also the extra repositories for each of the drivers of your vehicle’s sensors/actuators. To put the required code repositories in your catkin workspace `~/catkin_ws/src` in an easy way you can use a rosinstall file. You can download a rosinstall containing the pacakges needed for simulating a generic [SparusII](https://bitbucket.org/iquarobotics/cola2_wiki/src/24.01/rosinstalls/sparus2.rosinstall) or [Girona500](https://bitbucket.org/iquarobotics/cola2_wiki/src/24.01/rosinstalls/girona500.rosinstall) or use your particular rosinstall file if you have been provided one.

```bash
cd ~/catkin_ws/src
wstool init  # if you have not done it before
wstool merge path/to/downloaded/rosinstall/file
wstool update
```

Once all the repositories are in your catkin workspace you can compile them:

```bash
cd ~/catkin_ws/
catkin_make
```

Remember to always source your compiled workspace:

```bash
source ~/catkin_ws/devel/setup.bash
```

Or do it automatically every time you open a terminal with:

```bash
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
```

At this point COLA2 is ready to be run in simulation. You can proceed to the [basic\_usage](basic_usage.md) document to know how to start things up.
