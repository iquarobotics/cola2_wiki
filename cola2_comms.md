# Acoustic Communication

[TOC]

The acoustic communication with the vehicle is handled in two parts. At surface, the user communicates using the USBL system through the IQUAview user interface. On the vehicle side, the acoustic communications are handled by the `cola2_comms` package and the specific modem driver that is installed in the vehicle (e.g., `evologics_modem`). Figure 1 shows an overview of the acoustic communications system.

![Diagram of acoustic communications.](imgs/comms_diagram_vehicle.png)

Acoustic communication is achieved with TDM where all participants are required to be time-synchronized and the time is divided in slots. For each slot of time, only a single participant is allowed to start communication (Fig. 2).

![Diagram of TDM concept.](imgs/comms_tdm.png)

Commonly (Fig. 3), two slots with the same size are defined (i.e. \[3s, 3s\]) where the first slot (index 0) is reserved for the AUV and the second one (index 1) is reserved for the USBL, but other configurations and number of participants could also be defined.

![Commonly used TDM slots configuration.](imgs/comms_tdm_common.png)

When the USBL is in its slot, a single serialized structure is sent by IQUAview from surface (Table 1), and answered automatically with an `ack` by the vehicle. When this message is correctly transmitted, a USBL measurement is obtained at the top-side. The measurement is composed with the GPS signal that is also received in the control station, and the final computed position is sent in the next USBL slot to the AUV.

When the AUV is in its slot, a single serialized structure is sent by `cola2_comms` through the modem driver to the surface (Table 2).

| **Type** | **Description**                                               |
| -------- | ------------------------------------------------------------- |
| `char`   | Identifier of the message sender (`U`)                        |
| `double` | Timestamp of the data represented in this message             |
| `double` | GPS ⊕ USBL measured AUV position latitude                     |
| `double` | GPS ⊕ USBL measured AUV position longitude                    |
| `float`  | GPS ⊕ USBL measured AUV position depth                        |
| `float`  | Accuracy of the USBL measurement                              |
| `int32`  | Command to execute in vehicle (RecoveryAction, StartMission…) |
| `uint8`  | Unused                                                        |
| `uint8`  | Unused                                                        |

Comms message struct sent from the surface using USBL down to the AUV (total size 39 bytes).

| **Type** | **Description**                                                       |
| -------- | --------------------------------------------------------------------- |
| `char`   | Identifier of the message sender (`<V>`)                              |
| `double` | Timestamp of the data represented in this message                     |
| `double` | AUV navigator position latitude                                       |
| `double` | AUV navigator position longitude                                      |
| `float`  | AUV navigator position depth                                          |
| `float`  | AUV navigator heading                                                 |
| `int32`  | `status_code` from `safety_supervisor` node                           |
| `uint8`  | Altitude (encoded/decoded using `cola2_lib/comms/altitude.h`)         |
| `uint8`  | Elapsed time (encoded/decoded using `cola2_lib/comms/elapsed_time.h`) |

Comms message struct sent from the AUV to the top-side(total size 39 bytes).

## Modem driver

The `evologics_modem` node is a simple acoustic modem driver that receives the already serialized message that has to be sent through acoustic communications from the `/<vehicle>/comms/to_modem` topic and publishes the received acoustic communications to the `/<vehicle>/comms/from_modem` topic.

It also provides two topics to debug the exact instructions that are written to the modem (`/<vehicle>/evologics_modem/raw/to_modem`) and the ones that are read from the modem (`/<vehicle>/evologics_modem/raw/from_modem`).

Additionally, the Evologics modem driver in the vehicle adds the time in which the message is sent to the struct described in table 3. Then, when this message is recevied correctly at the top-side, the time difference can be computed to have an approximate estimation of the range till the AUV (provided that the controlstation and the vehicle PCs have been time synchronised). This range, together with the bearing-elevation measurement provided by the USBL can offer another rough estimation of the position of the AUV.

## Cola2\_comms

The `cola2_comms` node abstracts all the acoustic communication and simplifies the work for different modem driver implementations. The node is also in charge of executing the commands that are sent from surface (i.e. recovery\_actions). Moreover it offers the possibility to transfer extra custom data defined by the user (Tables 4 & 5).

| **Topic**                        | **Description**                            |
| -------------------------------- | ------------------------------------------ |
| `/<vehicle>/comms/to_modem`      | Serialized message to be sent by the modem |
| `/<vehicle>/comms/custom_output` | Custom user message received by the modem  |

`cola2_comms` published topics of type `std_msgs/String`.

| **Topic**                       | **Description**                             |
| ------------------------------- | ------------------------------------------- |
| `/<vehicle>/comms/from_modem`   | Serialized message received by the modem    |
| `/<vehicle>/comms/custom_input` | Custom user message to be sent by the modem |

`cola2_comms` subscribed topics of type `std_msgs/String`.

### Receiving messages

When a message is received through `/<vehicle>/comms/from_modem`, the message is first deserialized. Then, the identifier is checked to be `U` (from the surface) and the timestamp, position and accuracy are used to publish a USBL update to `/<vehicle>/navigator/usbl` topic for the `navigator` node.

After that, the received command is checked and immediately executed. If it refers to a `recovery_action`, the corresponding action is called. If the command refers to a start mission, the mission is started.

Finally, if the message is longer that the acoustic message struct, the extra bytes are published to the `/<vehicle>/comms/custom_output` topic. It is up to the user to process this serialized custom message that arrived from surface correctly.

### Sending messages

Messages are continuously sent to the modem driver, and it is the modem driver who manages which ones are actually sent through the acoustic channel. To create an output message, `cola2_comms` gathers navigation data from `/<vehicle>/navigator/navigation` and the vehicle status code from `/<vehicle>/safety_supervisor/status` to construct and serialize the message.

If a `/<vehicle>/comms/custom_input` was received in the specified period, the custom data is appended at the end of the serialized message. All together, is then sent to the modem driver through the `/<vehicle>/comms/to_modem` topic.

In the case of evologics modems, the modem driver adds the expected message emission timestamp to the message received from cola2\_comms to allow the surface receiver to compute an approximate range to the AUV. This timestamp is added as a `double` increasing the message size to 47 bytes.

### Custom messages

When developing nodes for custom messages (std\_msgs/String) in ROS Noetic, python3 nodes are limited to use valid ASCII characters. To send/recv binary data, c++ nodes have to be developed.

Users can easily define custom messages to be passed between the vehicle and the top side by creating their own node that serializes/deserializes them and collects and publishes the necessary data. This custom node should publish to the `/<vehicle>/comms/custom_input` topic in the vehicle and subscribe to the `/<vehicle>/comms/custom_output` topic in the vehicle.

The `cola2_comms` node ensures that when appending the custom message the limits of the maximum modem message size are respected and sends/receives transparently the custom data.

For an example on how to pass custom messages between the top-side control station and the vehicle you can have a look on this [tutorial](https://www.bitbucket.org/iquarobotics/cola2_tutorials/src/24.1.0/comms_custom_msgs/).
