# COLA2 WIKI (version 24.01)

Welcome to the COLA2 wiki!

COLA2 (Component Oriented Layer-based Architecture for Autonomy) is the [ROS](http://www.ros.org)-based software architecture running in the autonomous underwater vehicles from [Iqua Robotics](http://www.iquarobotics.com).

This Wiki will guide you through the installation of the main packages needed to run COLA2 in simulation, show you its basic usage and explain the basics of the main modules.

_Note: Through this wiki, `<vehicle>` refers equally to `girona500/sparus2` as well as the modem identifier `<V>` that corresponds to `G/S`._

## Index

* [Installation](basic_installation.md)
* [Basic usage](basic_usage.md)
* [Conventions](basic_conventions.md)
* `cola2_core` packages:
    * [cola2_navigation](cola2_nav.md)
    * [cola2_control](cola2_control.md)
    * [cola2_safety](cola2_safety.md)
    * [cola2_log](cola2_log.md)
    * [cola2_comms](cola2_comms.md)
    * [cola2_simulation](cola2_sim.md)

## Older versions of this wiki

* [20.10](https://bitbucket.org/iquarobotics/cola2_wiki/src/20.10/)
* [19.11](https://bitbucket.org/iquarobotics/cola2_wiki/src/201911/)
* [19.02](https://bitbucket.org/iquarobotics/cola2_wiki/src/201902/)
