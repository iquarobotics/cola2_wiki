# Safety

[TOC]

The `cola2_safety` is the ROS package in charge of the vehicle’s safety. It contains several nodes that monitor and supervise important values in the architecture. The COLA2 safety is organized around the standard ROS diagnostic messages. The diagnostics system is designed to collect information from hardware drivers, sensor status, communications and other important information useful for analysis, troubleshooting and logging.

All the important nodes in the architecture publish a diagnostic message to report whether they are operating normally or not, describing the cause of failure. These messages are of type `diagnostic_msgs/DiagnosticStatus`, published on the topic `/<vehicle>/diagnostics`. Using the standard ROS diagnostic aggregator node, all the messages are combined to a single diagnostic message that summarizes the main safety values and parameters of the architecture. The type of the aggregated message is `diagnostic_msgs/DiagnosticArray` and it is published on the topic `/<vehicle>/diagnostics_agg`. The `rqt_robot_monitor` tool can be used to visualize the aggregated diagnostic message:

```bash
rosrun rqt_robot_monitor rqt_robot_monitor diagnostics_agg:=/<vehicle>/diagnostics_agg
```

The classification of the diagnostics into different categories (e.g.: control, safety, navigation) is specified in the `diagnostics_aggregator.yaml` configuration file.

When a failure is detected, a safety level is determined according to the severity of the failure. Each safety level is associated with a particular recovery action, the goal of which is bringing the system back to a safe state. The possible safety levels are:

- *NONE (0)*: This safety level corresponds to the normal operation of the vehicle. No failure has been detected.

- *INFORMATIVE (1)*: The informative safety level is triggered when a minor failure is detected. The only action performed by this safety level is notifying the user by means of a message in the screen or the graphical user interface.

- *ABORT (2)*: When the abort safety level is activated, any goto, section, keep position, mission or external mission is disabled, so the robot is brought back to idle, waiting for further commands from the controlstation.

- *ABORT\_AND\_SURFACE (3)*: When the abort and surface safety level is activated, any goto, section, keep position, mission or external mission is disabled, and a safety keep position is enabled. The vehicle keeps position at the depth specified by the parameter `/<vehicle>/captain/safety_keep_position_depth` in the `captain.yaml` configuration file. The safety keep position can be performed at the current location, or alternatively, at the NED origin, by configuring the parameter `/<vehicle>/captain/safety_keep_position_goes_to_ned_origin`, also in the `captain.yaml` configuration file:

    ```yaml
    # Depth of the safety keep position
    safety_keep_position_depth: 0.0

    # If true, the safety keep position goes to the NED origin. If false, it just keeps position
    safety_keep_position_goes_to_ned_origin: true
    ```

- *EMERGENCY\_SURFACE (4)*: This safety level is used for major failures. For instance, when there is a failure that causes a malfunction of the navigation system of the vehicle. When it is activated, the performed recovery action is to surface by sending direct commands to the vehicle thrusters, bypassing all the major systems of the vehicle. The emergency setpoint is configured by the following parameter in the `safety_supervisor.yaml` configuration file:

    ```yaml
    # Thruster setpoints used to perform EMERGENCY_SURFACE [thruster 0,1,2]
    emergency_surface_setpoints: [-0.30, 0.0, 0.0]

    # Thruster setpoints used to perform EMERGENCY_SURFACE [thruster 0,1,2,3,4]
    emergency_surface_setpoints: [0.0, 0.0, 0.75, 0.75, 0.0]
    ```

    The user should only change this parameter when there is a major change in the thrusters configuration of the vehicle, and great care has to be put to ensure that the values and signs are correct to ensure that the vehicle surfaces.

    When the vehicle enters in emergency surface the thrusters will be actively pushing up until the reason that triggered the emergency behaviour ceases or manual teleoperation is enabled once the vehicle is at surface. Note that since navigation cannot be trusted in the emergency state, the teleoperation in that situation will be in open loop mode. If for some reason, (e.g. vehicle submerges during teleoperation) the connection is lost with the vehicle and teleoperation is not active, the vehicle will reenter in emergency surface state. The emergency behaviour always performs a smooth ramp until pushing up with the configured thrust, to warn nearby operators and avoid dangerous situations.

- *DROP\_WEIGHT (5)*: For the vehicles equipped with a drop weight system, activating this safety level will trigger the recovery action of dropping the weight.

In the following sections, a description of each safety node will be provided. The parameters used by the safety nodes are defined in `cola2_<vehicle>/config/`, in the corresponding `.yaml` files.

## Safety supervisor

The `safety_supervisor_node` is the most important safety node. The safety supervisor uses different safety rules to check the values of the aggregated diagnostic message. The goal of each rule is to check a portion of the diagnostic message, to report a safety level and a message, and to change the status code accordingly (see Table 1). The safety supervisor node captures the safety level and messages reported by all safety rules and reports the highest safety levels, with the messages corresponding to that level, which describe the failure or failures that cause the reported safety level. Figure 1 shows an overview of the main safety workflow in COLA2.

![Diagram of the diagnostics and safety supervisor system.](imgs/safety_diagram_vehicle.png)

The behavior of the safety supervisor node can be configured using the parameters found in the `safety_supervisor.yaml` file. A detailed description of the available safety rules and their parameters is provided:

- *BatteryLevel*: This safety rule checks the battery charge and voltage. When the battery level is close to the minimum, an informative message is generated. When it is below the minimum, an abort and surface recovery action is triggered. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "BatteryLevel"
    "rule_name"/min_charge: 14.5
    "rule_name"/min_voltage: 26.4
    ```

    With this configuration, the rule checks the values of `charge` and `voltage` of the diagnostic `/safety/"rule_name"`.

- *Captain*: This safety rule handles the current waypoint counter, filling the corresponding bits of the status code. This rule has no configuration parameters, so only the following has to be specified in the safety supervisor .yaml file:

    ```yaml
    "rule_name"/type: "Captain"
    ```

- *CheckEnabled*: This is a generic rule that is used to check whether a node is enabled or not. The rule accepts a list of nodes to check and the safety level that must be triggered when they are not enabled. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "CheckEnabled"
    "rule_name"/diagnostic_status_names_to_check: ["/safety/safe_depth_altitude"]
    "rule_name"/level_if_not_enabled: 3
    ```

    With this configuration, the safety rule checks the `enabled` value of the diagnostics specified in the list, which in this case corresponds to `/safety/safe_depth_altitude`. When the value `enabled` is false or not present, the safety level *ABORT\_AND\_SURFACE (3)* is activated, causing the abort and surface recovery action. Typically, many instances of this rule run simultaneously, checking different diagnostic values and activating different safety levels.

- *CombinedWaterInside*: This rule checks whether there is water inside the hulls or not. It is able to handle up to 3 hulls: the main hull of the vehicle, the battery hull and an external hull.

    For a Sparus robot this configuration typically is:

    ```yaml
    "rule_name"/type: "CombinedWaterInside"
    "rule_name"/check_battery_housing: false
    "rule_name"/check_extra_housing: false
    ```

    For a Girona robot this configuration typically is:

    ```yaml
    "rule_name"/type: "CombinedWaterInside"
    "rule_name"/check_battery_housing: true
    "rule_name"/check_extra_housing: false
    ```

    For a Girona robot with IxBlue INS this configuration typically is:

    ```yaml
    "rule_name"/type: "CombinedWaterInside"
    "rule_name"/check_battery_housing: true
    "rule_name"/check_extra_housing: true
    ```

    For a Girona robot this configuration typically is:

    ```yaml
    "rule_name"/type: "CombinedWaterInside"
    "rule_name"/check_battery_housing: true
    "rule_name"/check_extra_housing: false
    ```

    For a Girona robot this configuration typically is:

    ```yaml
    "rule_name"/type: "CombinedWaterInside"
    "rule_name"/check_battery_housing: true
    "rule_name"/check_extra_housing: false
    ```

- *Comms*: If the vehicle is equipped with acoustic communication, this rule is used to trigger a safety level from the controlstation at surface. At the same time, the rule also checks the elapsed time without acoustic communication. If the elapsed time without acoustic communication exceeds the specified timeout, the abort and surface safety level is triggered. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "Comms"
    "rule_name"/modem_data_timeout: 300.0
    ```

    The rule checks the values `last_modem_data` and `modem_recovery_action`, of the diagnostic `/safety/"rule_name"`.

- *Manual*: This rule allows the user to manually trigger a safety level. It has no configuration parameters, so only the following has to be specified in the safety supervisor .yaml file:

    ```yaml
    "rule_name"/type: "Manual"
    ```

    With this configuration, the rule creates a ROS service client, which allows to manually trigger a safety level, at `/<vehicle>/safety_supervisor/"rule_name"/set_level`.

- *Navigator*: This rule checks different values of the navigator, such as the data age of the different navigation sensors and whether the navigation filter is initialized or not. The configuration specifies the timeout for the different sensors and the minimum allowed frequency:

    ```yaml
    "rule_name"/type: "Navigator"
    "rule_name"/nav_data_timeout: 2.0
    "rule_name"/imu_data_timeout: 10.0
    "rule_name"/depth_data_timeout: 10.0
    "rule_name"/altitude_data_timeout: 10.0
    "rule_name"/dvl_data_timeout: 10.0
    "rule_name"/gps_data_timeout: 30.0
    "rule_name"/min_frequency: 20.0
    ```

    With this configuration, the rule checks the values `filter_init`, `last_imu_data`,
    `last_depth_data`, `last_altitude_data`, `last_dvl_data` and `last_gps_data` of the diagnostic `/navigation/"rule_name"`.

- *Teleoperation*: This rule checks the teleoperation link between the robot and the controlstation. If the link is not detected for long enough, the abort and surface safety level is triggered. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "Teleoperation"
    "rule_name"/teleoperation_link_timeout: 60.0
    ```

    The rule checks the value `last_ack` of the diagnostic `/control/"rule_name"`.
- *Temperature*: This generic rule is used to check the temperature of a sensor. If the temperature is higher than the configured maximum, the abort and surface safety level is triggered. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "Temperature"
    "rule_name"/diagnostic_status_name: ""diagnostic_status_name""
    "rule_name"/max_temperature: 100.0
    ```

    With this configuration, the rule checks the value `temperature` of the diagnostic specified by the status name `"diagnostic_status_name"`.

- *VirtualCage*: This rule is used to check if the robot has escaped the designated working area (virtual cage). When the robot is out of the virtual cage, this rule can trigger the abort and surface safety level. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "VirtualCage"
    "rule_name"/enables_abort_and_surface: true
    ```

    With this configuration, the rule checks the value `enables_abort_and_surface` of the diagnostic `/safety/"rule_name"`.

- *WatchdogTimer*: This rule implements the architecture timeout. When the timeout is exceeded, the abort and surface safety level is triggered. The configuration parameters of this safety rule are:

    ```yaml
    "rule_name"/type: "WatchdogTimer"
    "rule_name"/timeout: 3600
    ```

    With this configuration, the rule checks the value `elapsed_time` of the diagnostic specified by the status name `/safety/"rule_name"`.

A summary of the triggered rules in the `safety_supervisor` and the corresponding recovery action that is called in each case can be seen in the table 2.

| **Condition or failure**      | **Safety level**  | **Comments**                                        |
| ----------------------------- | ----------------- | --------------------------------------------------- |
| Low battery level             | INFORMATIVE       | Below 1.5\*battery level threshold                  |
| Battery level under threshold | ABORT AND SURFACE |                                                     |
| Voltage level under threshold | ABORT AND SURFACE |                                                     |
| No IMU data                   | EMERGENCY SURFACE |                                                     |
| No depth data                 | EMERGENCY SURFACE |                                                     |
| No altitude data              | ABORT AND SURFACE |                                                     |
| No DVL data                   | ABORT AND SURFACE |                                                     |
| No GPS data                   | INFORMATIVE       | Only checked if vehicle is in surface               |
| No navigation data            | EMERGENCY SURFACE |                                                     |
| No WiFi data                  | ABORT AND SURFACE | Only checked if vehicle is not in autonomous mode   |
| No modem data                 | ABORT AND SURFACE |                                                     |
| Water leak                    | ABORT AND SURFACE |                                                     |
| High temperature              | ABORT AND SURFACE |                                                     |
| Watchdog timer                | ABORT AND SURFACE |                                                     |
| Outside of virtual cage       | ABORT AND SURFACE | Only if virtual\_cage/calls\_keep\_position is true |

 Summary of triggered safety levels according to the different failures.

The safety supervisor publishes a `SafetySupervisorStatus` message, which includes the current `error_code` and safety level, to the topic `/<vehicle>/safety_supervisor/status`.

The `status_code` is a `std_msgs/Int32` message that codifies, bitwise, different status information, errors and warnings in four bytes. This `status_code` is used to pass information about the state of the vehicle in a compact way, for instance when using acoustic communication to know the state of the vehicle at surface. Table 3 shows the bit codification of the `status_code`.

| **Bit** | **Description**                    |
| ------- | ---------------------------------- |
| 0-7     | Mission step (0-255)               |
| 8       | High temperature error             |
| 9       | Water inside error                 |
| 10      | No altitude error                  |
| 11      | Navigation error                   |
| 12      | Watchdog timer                     |
| 13      | Battery error                      |
| 14      | Low battery warning                |
| 15      | Recovery action: abort mission     |
| 16      | Recovery action: abort and surface |
| 17      | Recovery action: emergency surface |
| 18      | Recovery action: dropweight        |
| 19      | Control board status indicator 1   |
| 20      | Control board status indicator 2   |
| 21-32   | TBD                                |

 Status code bit codification.

## Safety supervisor checker

The `safety_supervisor_checker_node` is a very simple node that checks that the safety supervisor node is alive. To do so, it checks that the safety supervisor status message is periodically published. If the safety supervisor crashes, the safety supervisor checker activates an emergency surface recovery action. At the same time, the safety supervisor checker publishes a diagnostic message that is checked by the safety supervisor, so both nodes check each other to provide redundant safety.

## Safe depth altitude

The `safe_depth_altitude_node` implements a reactive behavior to avoid colliding with the seabed or surpassing the maximum allowed depth. These two limit values are configured using the parameters `max_depth` and `min_altitude` in the `safe_depth_altitude.yaml`. When reaching the specified limits, this node generates a `cola2_msgs/BodyVelocityReq` message asking the vehicle a negative heave velocity with maximum priority (overriding manual operation), thus causing the vehicle to go up.

When no altitude is available, which happens when the seabed cannot be acoustically detected, the node can either send a request to go up or ignore the missing altitude data. By default, when the node starts, the behavior is configured to go up if no altitude is available. By means of two ROS services (`/<vehicle>/safe_depth_altitude/enable_no_altitude_goes_up` and `/<vehicle>/safe_depth_altitude/disable_no_altitude_goes_up`), this behavior is automatically changed by the `captain_node` according to its configuration parameter `idle_no_altitude_goes_up`, and its current state. When performing a mission, the behavior can be specified for each waypoint, so that it can be tailored to the environment as needed.

Additionally, the depth at which the minimum altitude threshold becomes effective can be controlled by the parameter `min_altitude_starts_at_depth`. This should only be changed in situations where the DVL gives incorrect bottom readings close to surface, in order to skip the problematic water layer and allow the vehicle to submerge.

## Virtual cage

The `virtual_cage_node` checks the position of the robot in NED coordinates, and publishes a diagnostic message reporting whether the robot is inside the delimited working area or not. The diagnostic published by this node is then used by the aforementioned virtual cage safety rule to activate the desired safety level. The cage has a circular shape defined by the parameters `cage_center_north`, `cage_center_east` and `cage_radius` in the `virtual_cage.yaml`

## Watchdog timer

The `watchdog_timer_node` measures the amount of time elapsed since the architecture has been started. By means of its `/<vehicle>/watchdog_timer/reset_timeout` ROS service, the timer can be restarted. The diagnostic published by this node is used by the aforementioned watchdog timer safety rule to implement the COLA2 architecture timeout.
